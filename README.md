# Code-Server Fedora edition OCI image

[code-server](https://github.com/codercom/code-server) based on Fedora

It's providing the same functionality as the original image but based on the latest stable Fedora

## Disclaimer

It's providing the same functionality as the original, you can find more information about it, on the offical repository of the [code-server](https://github.com/codercom/code-server).

## Infos

If you don't have a ```${HOME}/.local/share/code-server``` folder, you can make it with this command.
If you got such error message as ```no such identity``` you can solve with this command: ```ssh-add ~/.ssh/IDENTITY_NAME```, just change the ```IDENTITY_NAME``` to the missing one.
If you are using GPG signing with password, the password will be asked on the original terminal, where you started the server.

### For BASH

```bash
mkdir -p ${HOME}/.local/share/code-server
```

### For FISH

```bash
mkdir -p $HOME/.local/share/code-server
```

## How to use it with BASH

```bash
docker run --privileged -it \
 -p 127.0.0.1:8443:8443 \
 -v "${PWD}:/projects" \
 -v "${HOME}/.ssh:/home/coder/.ssh:ro" \
 -v "${HOME}/.gnupg:/home/coder/.gnupg" \
 -v "${HOME}/.local/share/code-server:/home/coder/.local/share/code-server"
 -t bloomflare/coder_fedora --allow-http --no-auth
```

## How to use it with FISH

```bash
docker run --privileged -it \
 -p 127.0.0.1:8443:8443 \
 -v "$PWD:/projects" \
 -v "$HOME/.ssh:/home/coder/.ssh:ro" \
 -v "$HOME/.gnupg:/home/coder/.gnupg" \
 -v "$HOME/.local/share/code-server:/home/coder/.local/share/code-server"
 -t bloomflare/coder_fedora --allow-http --no-auth
```

## License

[MIT](LICENSE)