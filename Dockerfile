FROM fedora:latest

WORKDIR /root

# Install necessary applications and tools
RUN yum install git openssl net-tools dumb-init sudo vim gpg pinentry procps -y

# Declare constants
ENV NVM_DIR /usr/local/nvm
ENV NVM_VERSION v0.34.0
ENV NODE_VERSION v10.15.3
ENV CODE_SERVER_VERSION 1.903-vsc1.33.1

# Create the necessary folders
RUN mkdir -p $NVM_DIR

# Install Node Version Manager
RUN curl -L https://raw.githubusercontent.com/creationix/nvm/${NVM_VERSION}/install.sh | sh
RUN /bin/bash -c "source $NVM_DIR/nvm.sh && nvm install $NODE_VERSION && nvm use --delete-prefix $NODE_VERSION"

# Set enviroment paths
ENV NODE_PATH $NVM_DIR/versions/node/$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/$NODE_VERSION/bin:$PATH

# Create a new user
RUN useradd -rm -d /home/coder -s /bin/bash -g root -G wheel -u 1000 coder \
  && echo "coder ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/nopasswd
USER coder

# Create our projects folder
RUN sudo mkdir -p /projects \
  && sudo chmod g+rw /projects

# Create our extensions folder
RUN sudo mkdir -p /home/coder/.local/share/code-server \
  && sudo chmod g+rw /home/coder/.local/share/code-server

# Set work directory to the home directory
WORKDIR /projects

# Install code-server
RUN curl -L https://github.com/codercom/code-server/releases/download/${CODE_SERVER_VERSION}/code-server${CODE_SERVER_VERSION}-linux-x64.tar.gz \
  -o server.tar.gz
RUN tar -xzf server.tar.gz
RUN sudo mv /projects/code-server${CODE_SERVER_VERSION}-linux-x64/code-server /usr/local/bin/code-server
RUN sudo chmod +x /usr/local/bin/code-server

# Copy startup file
COPY startup.sh /home/coder/startup.sh

# Cleanup
RUN rm -rf *

# This assures we have a volume mounted even if the user forgot to do bind mount.
VOLUME [ "/projects" ]
VOLUME [ "/home/coder/.local/share/code-server" ]
VOLUME [ "/home/coder/.ssh" ]
VOLUME [ "/home/coder/.gnupg" ]

EXPOSE 8443
ENTRYPOINT ["dumb-init", "/home/coder/startup.sh"]
